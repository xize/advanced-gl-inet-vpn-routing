#!/bin/sh

install() {
	echo 'installing gl-inet advanced-gl-inet-vpn-routing script'
	echo 'installing bash and git!...'
	opkg update
	opkg install bash
	clear
	echo 'installing done!, downloading scripts!'
	curl -Sso vpnutil.sh https://gitlab.com/xize/advanced-gl-inet-vpn-routing/-/raw/main/util/vpnutil.sh #script 1 [placeholder]
	curl -Sso route_policy https://gitlab.com/xize/advanced-gl-inet-vpn-routing/-/raw/main/util/route_policy #script 2 [placeholder]

	if ! test -f "/usr/bin/route_policy_original"; then 
		mv /usr/bin/route_policy /usr/bin/route_policy_original
	fi

	cp -r route_policy /usr/bin/route_policy
	chmod +x /usr/bin/route_policy
	cp -r vpnutil.sh /usr/bin/vpnutil.sh
	chmod +x /usr/bin/vpnutil.sh
	
	echo 'the script has been installed!, now please reboot the router and check the logs in luci :)'
}

uninstall() {
	echo 'uninstalling gl-inet-vpn-routing script!'
	#sanity check.
	if test -f "/usr/bin/route_policy_original"; then
		rm -r "/usr/bin/route_policy"
		mv -r /usr/bin/route_policy_original /usr/bin/route_policy
		rm -r /usr/bin/vpnutil.sh > /dev/null # we ignore any output because maybe the file was already deleted.
	else
		echo 'the script seemed not to be installed!'
	fi
}

if [ ! -z "$1" ]
then
	if [ "$1" == "install" ]
	then
		install
	elif [ "$1" == "uninstall" ]
	then
		uninstall
	else
		echo "argument options: install|uninstall"
	fi
fi
#!/bin/bash

. /lib/functions.sh

ignore_iface_list=("eth0" "eth1" "eth2" "eth3" "eth4" "lo") #add wgclient/tun0 if something in the future changes.
declare -a iface_list=()

handleIfaces() {
    config_get devname "$1" 'device'
	config_get devip "$1" 'ipaddr'
	
	if [ ! -z "$devname" ] # check if its null and also does not contain in our ignore list.
	then
		if [[ ! "${ignore_iface_list[@]}" =~ "${devname}" ]]
		then
			logger -t "vpn policy routing" "registering interface for vpn policies: $devname,dhcp $devip"
			iface_list+=("${devname}")
		fi
	fi
}

LoadInterfaceAwareness() {
	iface_list=() #empty the list first.
	logger -t "vpn policy routing" "== [Checking VPN Policy interface awareness!] =="
	config_load network # load the configuration
	config_foreach handleIfaces interface
}

GetInterfaces() {
	return $iface_list
}